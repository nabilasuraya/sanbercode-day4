<?php

require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun");
echo "Nama Binatang : $sheep->name <br>"; 
echo "Jumlah Kaki : $sheep->legs <br>" ; 
echo "Berdarah dingin ? : $sheep->cold_blooded <br> <br>" ;


$kodok = new Frog("buduk");
echo "Nama Binatang : $kodok->name <br>"; 
echo "Jumlah Kaki : $kodok->legs <br>" ; 
$kodok->jump() ; 


$sungokong = new Ape("kera sakti");
echo "Nama Binatang : $sungokong->name <br>";
echo "Jumlah Kaki : $sungokong->legs <br>" ; 
$sungokong->yell() ;
//git remote --v
?>

